package br.com.senac.ciclodevidaactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("#CICLO", "OnCreate Rodando...");
    }
    @Override
    protected void onStart() {
        super.onStart();

        Log.i("#Ciclo", "OnStart Rodando...");
    }
    @Override
    protected void onResume(){
        super.onResume();
        Log.i("#CiCLO","OnResume Rodando...");


    }
    @Override
    protected void onPause(){
        super.onPause();
        Log.i("#CICLO","OnPause Rodando...");
    }
    @Override
    protected void onStop(){
        super.onStop();
        Log.i("#CICLO","OnStop Rodando...");
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.i("#CICLO","OnDestroy Rodando...");

    }
    @Override
    protected void onRestart(){
        super.onRestart();
        Log.i("#CICLO","OnRestart Rodando...");
    }
}
